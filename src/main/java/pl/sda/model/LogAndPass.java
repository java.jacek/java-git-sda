package pl.sda.model;

public class LogAndPass {
    private String log;
    private String pass;

    public String getLog() {
        return log;
    }

    public String getPass() {
        return pass;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}

