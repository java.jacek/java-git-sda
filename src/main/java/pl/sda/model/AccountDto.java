package pl.sda.model;

import java.math.BigDecimal;

public class AccountDto {
    private String number;
    private BigDecimal balance;
    private boolean active;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "AccountDto{" +
                "number='" + number + '\'' +
                ", balance=" + balance +
                ", active=" + active +
                '}';
    }
}
