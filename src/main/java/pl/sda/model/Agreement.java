package pl.sda.model;

import java.util.Arrays;

public class Agreement {
    private String name;
    private String signature;
    private String[] consents;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String[] getConsents() {
        return consents;
    }

    public void setConsents(String[] consents) {
        this.consents = consents;
    }

    @Override
    public String toString() {
        return "Agreement{" +
                "name='" + name + '\'' +
                ", signature='" + signature + '\'' +
                ", consents=" + Arrays.toString(consents) +
                '}';
    }
}
