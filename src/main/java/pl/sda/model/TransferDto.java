package pl.sda.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class TransferDto {
    private AccountDto from;
    private AccountDto to;
    private BigDecimal amount;
    private LocalDateTime date;

    public AccountDto getFrom() {
        return from;
    }

    public void setFrom(AccountDto from) {
        this.from = from;
    }

    public AccountDto getTo() {
        return to;
    }

    public void setTo(AccountDto to) {
        this.to = to;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "TransferDto{" +
                "from=" + from +
                ", to=" + to +
                ", amount=" + amount +
                ", date=" + date +
                '}';
    }
}
